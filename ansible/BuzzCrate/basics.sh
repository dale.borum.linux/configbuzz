#! /usr/bin/bash

cat <<EOF
When prompted (assuming users have not been modified), 
please enter in the following passwords. 
SSH Password: alarm
root password: root

This must be entered for every change needing to be made. 

EOF

ansible nodes -m "raw" -a "pacman-key --init" -k -K -u alarm --become --become-method "su"
ansible nodes -m "raw" -a "pacman-key --populate archlinuxarm" -k -K -u alarm --become --become-method "su"
ansible nodes -m "raw" -a "pacman -Syy" -k -K -u alarm --become --become-method "su"
ansible nodes -m "raw" -a "pacman -Su --noconfirm" -k -K -u alarm --become --become-method "su"
ansible nodes -m "raw" -a "pacman -S python --noconfirm" -k -K -u alarm --become --become-method "su"
